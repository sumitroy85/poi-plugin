package com.rayzware.poi.plugin;

/**
 * Enumeration constants for TAB and COMMA
 *
 * @author - Sumit Roy
 * @Created On - 5 May, 2017,6:13:47 PM
 * @Project - poi-plug in
 */
public enum Delimiter {

    COMMA(","), // 0
    TAB("\t");  // 1
    
    private final String delimiter;

    private Delimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    @Override
    public String toString() {
        return delimiter;
    }
}
