package com.srsoft.poi.plugin.test;

/**
 *
 * @author - Sumit Roy
 * @Created On - 25 Apr, 2017,11:10:08 AM
 * @Project - poi-plugin
 */
public enum Subject {
    ENGLISH("English"), // 0
    MATHEMATICS("Mathematics"), // 1
    PHYSICS("Physics"), // 2
    CHEMISTRY("Chemistry"), // 3
    BIOLOGY("Biology");  // 4 

    private final String subjectName;

    private Subject(String themeName) {
        this.subjectName = themeName;
    }

    @Override
    public String toString() {
        return subjectName;
    }
}
